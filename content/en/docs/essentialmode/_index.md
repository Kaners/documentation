---
title: "EssentialMode"
---

{{% alert title="Support Development" color="secondary" %}}
Become a patron today at our [Patreon](https://patreon.com/gdevelopment) and get access to personal support
{{% /alert %}}

### Description

EssentialMode is a framework for FiveM for resources to communicate to eachother and have a common ground to access and save user data. EssentialMode was initially released in january of 2017 and is one of the most used FiveM resources and most used framework for FiveM.

### Features

- Advanced permission system, groups and specific levels
- Easily add commands
- Logs for almost everything
- Made for perfection
- Clean readable code
- User class for easy user manipulation and saving
- Many available resources
- Compatibility made easy

### History

Started in mid 2016 as just being called "Freeroam" it was started as a gamemode to clone GTA onlines functionality, however it quickly grew to be more of its own thing as things grew. Including things like business and house ownership also being able to purchase vehicles and more, but still keeping it's own flair and not becoming a roleplay gamemode as it had many things that would never work in RP. Like on the fly vehicle repairs anywhere you are at a cost.

When freeroam became increasingly harder to manage I decided to write a version 2 for it, which was also the first release I ever did for FiveM(named FiveReborn at the time). It had all the features from v1 but was way more optimized and neatly working, there was however one major flaw: "synchronous database saving". I was saving and loading, basically any database operating I did was on the main thread. Which resulted in disconnects, desync and just overall buggyness. So when everything failed I wanted to write a framework for future gamemodes to rely on in which I decided to start writing "EssentialMode" so when I ever did something stupid again I just had to fix the framework and my gamemodes and other resources would continue to function.

This went on for a while until version 4, when CitizenMP.Server was being replaced by FXServer. This happened in june of 2017 and broke literally every resource and gamemode depending on EssentialMode. This was to do with NeoLua being dropped and a new system being introduced, this was something I couldn't avoid and every resource and gamemode needed some updates to it's interaction with EssentialMode. Luckily we're passed this stage.

Since EssentialMode 4 everything has mostly been good, just a few hiccups here and there but nothing major. But since that version no one had to drastically change their resources to work again and I'm actually quite happy about that.