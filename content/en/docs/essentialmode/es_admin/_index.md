---
title: "Admin System"
weight: 1005
---

es_admin

Download: [https://github.com/kanersps/es_admin](https://github.com/kanersps/es_admin)

EssentialMode admin is an administration resource for FiveM which has many functions to ease up administration of your FiveM server. As it's name suggests it also has support for administrating your EssentialMode installation.
Features

    Player administration
    Setting player roles
    Setting player groups
    Automatically change ACL
    Teleporting functions
    Freezing
    Kicking
    Banning
    Ban times
    Slaying players
    Crashing players (legacy, might be removed anytime)
    Admin GUI ingame
    Noclip

+more

### Banning

Banning a user is straightforward and can be done either via the GUI, which will always be a permanent ban or by a command. The command is easy, just do this:

`/ban ID TIME REASON HERE`

So to ban user 5 for 10 seconds with a reason "Banned for misbehaving"

`/ban 5 10 Banned for misbehaving`

And to ban a user for 10 days

`/ban 5 10d Banned for misbehaving`

The allowed time operations are:

    Minutes (m)
    Hours (h)
    Days (d)
    Years (y)

You can only use one of them at a time.
### History

EssentialMode admin was the first addon released for EssentialMode. I don't exactly remember when it was made as I'm quite sure I made it in close relation to EssentialMode but it was released the same time as EssentialMode itself. In fact it was bundled with EssentialMode for a very long time and therefore I consider them to be one of the most compatible resources with EssentialMode.

While making es_admin I saw that many people were craving more simplistic administration possibilities so soon after the first release I already started working on version 2. Version 2 had everything version 1 had but it had one major difference: "GUI support", it was now possible to use es_admin in it's full extend(and even slightly more than with commands) with a full GUI experience. You can click on users to change their permission levels and kick users and far far more then that.

With release of version 2 I was quite happy with what es_admin was and didn't update it for a long long time. Even now I just do bugfixes but maybe sometime in the future I'll release another big addition to es_admin