---
title: "Settings"
---

Settings in use by EssentialMode

```lua
-- es:setDefaultSettings
 
TriggerEvent('es:setDefaultSettings', {
    ['pvpEnabled'] = false,
    ['permissionDenied'] = GetConvar('es_permissionDenied', 'false'),
    ['startingCash'] = GetConvar('es_startingCash', '0'),
    ['startingBank'] = GetConvar('es_startingBank', '0'),
    ['enableRankDecorators'] = GetConvar('es_enableRankDecorators', 'false'),
    ['moneyIcon'] = GetConvar('es_moneyIcon', '$'),
    ['nativeMoneySystem'] = GetConvar('es_nativeMoneySystem', '1'),
    ['commandDelimeter'] = GetConvar('es_commandDelimeter', '/'),
    ['enableLogging'] = GetConvar('es_enableLogging', 'false'),
    ['enableCustomData'] = GetConvar('es_enableCustomData', 'false')
})
```

You can set all of these using convars

```
set es_permissionDenied "Pesky rat! You don't have permission for this!!"
set es_startingCash 10000
set es_startingBank 10000
set es_enableRankDecorators 1
set es_moneyIcon "-"
set es_nativeMoneySystem 1
set es_commandDelimeter "/"
set es_enableLogging 1
set es_enableCustomData 1
```

### Legacy
These are related to CouchDB, they are still here for reference

```lua
-- Change to wherever your DB is hosted, use convar
ip = GetConvar('es_couchdb_url', '127.0.0.1')

-- Change to whatever port you have CouchDB running on, use convar
port = GetConvar('es_couchdb_port', '5984')

-- "user:password", if you have auth setup, use convar
auth = GetConvar('es_couchdb_password', 'root:1202')
```

Example:

```
set es_couchdb_url "127.0.0.1"
set es_couchdb_port 3333
set es_couchdb_password "username:password"
```
