---
title: "Unknown"
weight: 1000
---

These versions didn't have a "version number" attached, probably as I was awful at keeping track of these things back then.

### Versions

18 Jan 2017 

    Performance improvement regarding having tons of commands(1000s), this will work fine now.
    New event: TriggerEvent('es:getAllPlayers', function(players) end) gives all the players even the offline ones.
    Misc stability improvements

25 Apr 2017 

    Added moneyIcon
    Fixed newPlayerLoaded event

 03 May 2017 

    Re-did database
    Async requests

Removed:

    Ban table
    getAllPlayers event

07 May 2017 

    Added exposed DB functions for custom plugin data
    Added user:setBankBalance(amount)

11 May 2017 

    Added the ability to set auth for CouchDB
    Slight code cleanup
    Added IP variable in db.lua so you can change the host of CouchDB
    Documentation done for EssentialMode
    Other stuff I forgot