---
title: "5.0.0"
weight: 920
---

19 Sep 2017 
### Changelog

Note: Fully supports es4.x >

Added

    Global banlist, can be disabled using es_admin2_globalbans (es_admin2)
    Full support for FXServer built in permissions
    Roles (For more info read down)

Updated

    Permission fixes related to groups
    Session var idType automatically assigned based on what ID was used for user loading
    License fixes

### Roles

EssentialMode now has a new type of permissions built in called roles , for this to work on custom data models please make sure to handle roles as a data column

Roles adds 3 new functions, also note that roles are permanent until they are removed.
```lua
user.addRole("role")
user.removeRole("role")
user.hasRole("role")
```
Note these are synchronously added but obviously saved asynchronously.
### Events

No changes

**Compatibility issues**

No changes