---
title: "RedEM & RedEM:RP"
---

<div style="display: flex;">
<div style="width: 50%; display: inline-block; float: left">
{{% alert title="RedEM: Web Administration Panel" color="default" %}}
Become a patron today at our [Patreon](https://patreon.com/gdevelopment) and get access to an exclusive Web Administration Panel to manage your RedEM:RP server. More information, click [here](redem/webpanel)
{{% /alert %}}
</div>
<div style="width: 50%; display: inline-block;">
{{% alert title="Digital Ocean Offer" color="default" %}}
Click [here](https://m.do.co/c/8329d881273b) for **$100** of free credit at Digital Ocean and support our development
{{% /alert %}}
</div>
</div>

Join our official RedEM: RP Discord for the latest updates, discussions and help: [https://discord.gg/nbmTmZR](https://discord.gg/nbmTmZR)